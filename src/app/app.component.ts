import {Component} from '@angular/core';
import {CurrencyService} from './services/currency.service';
import {Subject} from 'rxjs/Subject';
import {debounceTime, distinctUntilChanged} from 'rxjs/operators';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  private searchText$: Subject<any> = new Subject<any>();
  wallet: string;
  address: {
    isEthereum: boolean,
    isBitcoin: boolean,
    invalid: boolean
  };

  constructor(private service: CurrencyService) {
    this.searchText$.pipe(
      debounceTime(500),
      distinctUntilChanged(),
    ).subscribe(address => this.service.search(address));

    this.service.inputState$.subscribe(address => this.address = address);
    this.service.wallet$.subscribe(wallet => this.wallet = wallet);
  }

  search(address: string) {
    this.searchText$.next(address);
  }

}
