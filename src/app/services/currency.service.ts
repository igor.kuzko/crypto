import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Subject} from 'rxjs/Subject';
import * as WAValidator from 'wallet-address-validator';

@Injectable()
export class CurrencyService {
  inputState$: Subject<any> = new Subject<any>();
  wallet$: Subject<any> = new Subject<any>();

  constructor(private http: HttpClient) { }

  search(address) {
    const isEthereum = WAValidator.validate(address, 'ethereum');
    const isBitcoin = WAValidator.validate(address, 'bitcoin');

    this.wallet$.next(null);
    this.inputState$.next({
      isEthereum,
      isBitcoin,
      invalid: !(isEthereum || isBitcoin)
    });

    if (isEthereum) {
      this.getEthereumInfo(address).subscribe((response: any) => {
        this.wallet$.next(`${this.transformValue(response.result, 18)} Ether`);
      });
    } else if (isBitcoin) {
      this.getBitcoinInfo(address).subscribe((response: any) => {
        this.wallet$.next(`${this.transformValue(response[address].final_balance, 8)} BTC`);
      });
    }
  }

  getEthereumInfo(address) {
    const apiKey = 'HKYJDF4F7TT4ZX4ACZR8SDI3BARW5T6S24';
    return this.http.get(`https://api.etherscan.io/api?module=account&action=balance&address=${address}&tag=latest&apikey=${apiKey}`);
  }

  getBitcoinInfo(address) {
    return this.http.get(`https://blockchain.info/balance?active=${address}&cors=true`);
  }

  transformValue(value, count) {
    let result = '' + value;
    if (result.length < count + 1) {
      result  = '0.' + result;
    } else {
      result = result.substring(0, result.length - count) + '.' + result.substring(result.length - count);
    }
    return +result;
  }

}
